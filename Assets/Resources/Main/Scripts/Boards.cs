﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Boards : MonoBehaviour {

	// Use this for initialization
	[HideInInspector]
	public int width,height;
	public GameObject baseGrid;
	public Vector2 bottomLeft;


	void Start () {
		bottomLeft = new Vector2 (-2.3f, -4.5f);
		width = Setup.width;
		height = Setup.height;
		init ();
	}


	void init(){
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				GameObject grid = Instantiate (baseGrid);
				grid.transform.position = new Vector3 (grid.GetComponent<SpriteRenderer> ().sprite.bounds.size.x * i + i * 0.5f + bottomLeft.x , grid.GetComponent<SpriteRenderer> ().sprite.bounds.size.y * j + j * 0.5f + bottomLeft.y, 0f);
				Debug.Log (grid.GetComponent<SpriteRenderer> ().sprite.bounds.size/3);
				grid.name = i + " " + j;
			}
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
